#!/usr/bin/env bash
##~---------------------------------------------------------------------------##
##                        _      _                 _   _                      ##
##                    ___| |_ __| |_ __ ___   __ _| |_| |_                    ##
##                   / __| __/ _` | '_ ` _ \ / _` | __| __|                   ##
##                   \__ \ || (_| | | | | | | (_| | |_| |_                    ##
##                   |___/\__\__,_|_| |_| |_|\__,_|\__|\__|                   ##
##                                                                            ##
##  File      : install.sh                                                    ##
##  Project   : shellscript_utils                                             ##
##  Date      : Feb 24, 2018                                                  ##
##  License   : GPLv3                                                         ##
##  Author    : stdmatt <stdmatt@pixelwizards.io>                             ##
##  Copyright : stdmatt - 2018, 2019                                          ##
##                                                                            ##
##  Description :                                                             ##
##    This will install the libary in a place that other scripts              ##
##    would be able to "source" it.                                           ##
##---------------------------------------------------------------------------~##

##----------------------------------------------------------------------------##
## Variables                                                                  ##
##----------------------------------------------------------------------------##
DEV_MODE="false";


##----------------------------------------------------------------------------##
## Functions                                                                  ##
##----------------------------------------------------------------------------##
get_script_dir()
{
    local SCRIPT_DIR="$(cd "$(dirname "${BASH_SOURCE[1]}")" && pwd)";
    echo "$SCRIPT_DIR";
    return 0;
}


##----------------------------------------------------------------------------##
## Script                                                                     ##
##----------------------------------------------------------------------------##
##
## We're sourcing ourselves to let us use some library's functions.
source "$(get_script_dir)/src/main.sh";

## @NOTICE(stdmatt): On windows and OSX we don't have
## /usr/local/src so we need to create it... :(
if [ "$(pw_os_get_simple_name "$PW_WSL_AS_GNU_LINUX")" != "$(PW_OS_GNU_LINUX)" ]; then
    if [ ! -d "$PW_SHELLSCRIPT_UTILS_INSTALL_PATH" ]; then
        mkdir -p "$PW_SHELLSCRIPT_UTILS_INSTALL_PATH";
    fi;
fi;

pw_can_write_at_path "$PW_SHELLSCRIPT_UTILS_INSTALL_PATH";
test $? != 0 && pw_log_fatal "Directory ($PW_SHELLSCRIPT_UTILS_INSTALL_PATH) is not writeable...";


##
## Create the intallation directory.
rm    -rf "$PW_SHELLSCRIPT_UTILS_FULL_PATH";
mkdir -vp "$PW_SHELLSCRIPT_UTILS_FULL_PATH";

##
## Copy every file from src directory to the installtion directory.
LINK_OR_COPY="cp -v";
if [ "$DEV_MODE" == "true" ]; then
    echo "Development mode installtion - Linking file instead of copying.";
    LINK_OR_COPY="ln -v";
fi;

for ITEM in $(ls src/*.sh); do
    $LINK_OR_COPY $ITEM $PW_SHELLSCRIPT_UTILS_FULL_PATH;
done;

##
## Self run to test if everything is ok.
if [ -f "$PW_SHELLSCRIPT_UTILS_FULL_PATH/main.sh" ]; then
    echo $(pw_FG "Done") "- Installation sucessful!";
else
    echo "Installation failed...";
fi;
