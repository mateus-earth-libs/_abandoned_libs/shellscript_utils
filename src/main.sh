#!/usr/bin/env sh
##~---------------------------------------------------------------------------##
##                                                                            ##
##    _____ __ __ __ _____ __       _ _ _ __ _____ _____ _____ ____  _____    ##
##   |  _  |  |  |  |   __|  |     | | | |  |__   |  _  | __  |    \|   __|   ##
##   |   __|  |-   -|   __|  |__   | | | |  |   __|     |    -|  |  |__   |   ##
##   |__|  |__|__|__|_____|_____|  |_____|__|_____|__|__|__|__|____/|_____|   ##
##                           http://pixelwizards.io                           ##
##                                                                            ##
##  File      : main.sh                                                       ##
##  Project   : pw_shellscript_utils                                          ##
##  Date      : Feb 24, 2018                                                  ##
##  License   : GPLv3                                                         ##
##  Author    : stdmatt <stdmatt@pixelwizards.io>                             ##
##  Copyright : pixelwizards - 2018                                           ##
##                                                                            ##
##  Description :                                                             ##
##    Utilities to make the shellscripting a bit easier.                      ##
##    Currently we have functions to deal with:                               ##
##      - Colors (Put/Remove color escape sequences on string)                ##
##      - Error Handling (Very crude at it is now)                            ##
##      - Logging                                                             ##
##      - Path Manipulation                                                   ##
##      - Text Manipulation                                                   ##
##                                                                            ##
##    The libary will be expanded as the need to do so.                       ##
##    As always you're free to hack it                                        ##
##---------------------------------------------------------------------------~##

PW_SHELLSCRIPT_UTILS_INSTALL_PATH="/usr/local/src";
PW_SHELLSCRIPT_UTILS_ORG_PATH="${PW_SHELLSCRIPT_UTILS_INSTALL_PATH}/stdmatt";
PW_SHELLSCRIPT_UTILS_PROGRAM_PATH="${PW_SHELLSCRIPT_UTILS_ORG_PATH}/shellscript_utils";
PW_SHELLSCRIPT_UTILS_FULL_PATH="$PW_SHELLSCRIPT_UTILS_PROGRAM_PATH";

##----------------------------------------------------------------------------##
## Initialization                                                             ##
##----------------------------------------------------------------------------##
###-----------------------------------------------------------------------------
### @brief Gets the fullpath of the directory that the script resides.
### @note
###   The returned path is always from the script that is calling this
###   function, no matter if it's "sourced" or not.
### @returns
###   [echo] The fullpath of the function caller script. \n
###   [real] 0 if operation is succeeded, 1 otherwise.
### @NOTES:
###   This function needs to be here, because it's used in the initialization
###   scripts, nevertheless the real placement of it would be near the path
###   functions.
###
###   The previous approach of creating a "private" function and make this
###   call the "private" one happens to be problematic as the script directory
###   is reported differently.
###
###   For example, calling the "private" function thru the public one reports
###   the script dir of this file, not the file that source it.
###
###   Imagine that we have a file A.sh that source this file. on A.sh
###   we call pw_get_script_dir that by itself calls the "private" function,
###   it will report the path of this file, but calling the "private" function
###   directly reports the path of the A.sh.
pw_get_script_dir()
{
    local SCRIPT_DIR="$(cd "$(dirname "${BASH_SOURCE[1]}")" && pwd)";
    echo "$SCRIPT_DIR";
}

###-----------------------------------------------------------------------------
_pw_discover_running_machine()
{
    local _UNAME_VALUE=$(uname);
    local SCRIPT_DIR="$(pw_get_script_dir)";

    ## OSX.
    if [ -n "$(echo "$_UNAME_VALUE" | grep "Darwin")" ]; then
        source "${SCRIPT_DIR}/osx.sh";
    ## GNU/Linux.
    elif [ -n "$(echo "$_UNAME_VALUE" | grep "Linux")" ]; then
        ## We might be running under WSL, so let's check it.
        local _IS_WSL=$(uname -r | grep "Microsoft");
        if [ -n "$_IS_WSL" ]; then
            source "${SCRIPT_DIR}/wsl.sh";
        else
            source "${SCRIPT_DIR}/gnu_linux.sh";
        fi;
    ## Windows.
    else
        source "${SCRIPT_DIR}/win32.sh";
    fi;
}

_pw_discover_running_machine;


##----------------------------------------------------------------------------##
## VERSION                                                                    ##
##----------------------------------------------------------------------------##
###-----------------------------------------------------------------------------
_pw_mkversion()
{
   echo "$1.$2.$3";
}

###-----------------------------------------------------------------------------
PW_SHELLSCRIPT_UTILS_VERSION_MAJOR="1";
PW_SHELLSCRIPT_UTILS_VERSION_MINOR="3";
PW_SHELLSCRIPT_UTILS_VERSION_REVISION="0";

PW_SHELLSCRIPT_UTILS_VERSION=$(_pw_mkversion \
    $PW_SHELLSCRIPT_UTILS_VERSION_MAJOR      \
    $PW_SHELLSCRIPT_UTILS_VERSION_MINOR      \
    $PW_SHELLSCRIPT_UTILS_VERSION_REVISION   \
);

###-----------------------------------------------------------------------------
pw_get_version()
{
    echo "$PW_SHELLSCRIPT_UTILS_VERSION";
}


##----------------------------------------------------------------------------##
## Array                                                                      ##
##----------------------------------------------------------------------------##
###-----------------------------------------------------------------------------
### @brief Creates an array with the given arguments.
### @param $1 - [OUT]      The name of variable that will refer to the new array.
### @param $@ - [Optional] The values that will be added on the new array.
### @returns The size of the created array.
pw_array_create()
{
    local COMPONENTS=$(pw_count_words $@);
    local INDEX=0;
    local REAL_INDEX=0;
    local _ARR="$1";

    for ITEM in $@; do
        if [ $INDEX == 0 ]; then
            INDEX=$(( INDEX + 1 ))
            continue;
        fi;

        eval $_ARR[$REAL_INDEX]="$ITEM";
        INDEX=$(( INDEX + 1 ))
        REAL_INDEX=$(( REAL_INDEX + 1 ))
    done;

    return $REAL_INDEX;
}


##----------------------------------------------------------------------------##
## Colors                                                                     ##
##----------------------------------------------------------------------------##
##------------------------------------------------------------------------------
## Constants
_pw_CL_RST="\033[0m"

## Foreground - Normal
_pw_FG_K="\033[030m"
_pw_FG_R="\033[031m"
_pw_FG_G="\033[032m"
_pw_FG_Y="\033[033m"
_pw_FG_B="\033[034m"
_pw_FG_M="\033[035m"
_pw_FG_C="\033[036m"
_pw_FG_W="\033[037m"
## Foreground - Bright
_pw_FG_BK="\033[090m"
_pw_FG_BR="\033[091m"
_pw_FG_BG="\033[092m"
_pw_FG_BY="\033[093m"
_pw_FG_BB="\033[094m"
_pw_FG_BM="\033[095m"
_pw_FG_BC="\033[096m"
_pw_FG_BW="\033[097m"

## Background - Normal
_pw_BG_K="\033[40m"
_pw_BG_R="\033[41m"
_pw_BG_G="\033[42m"
_pw_BG_Y="\033[43m"
_pw_BG_B="\033[44m"
_pw_BG_M="\033[45m"
_pw_BG_C="\033[46m"
_pw_BG_W="\033[47m"
## Background - Bright
_pw_BG_BK="\033[100m"
_pw_BG_BR="\033[101m"
_pw_BG_BG="\033[102m"
_pw_BG_BY="\033[103m"
_pw_BG_BB="\033[104m"
_pw_BG_BM="\033[105m"
_pw_BG_BC="\033[106m"
_pw_BG_BW="\033[107m"

##------------------------------------------------------------------------------
## Functions
## Foreground - Normal
pw_FK() { echo -e "${_pw_FG_K}$@${_pw_CL_RST}"; }
pw_FR() { echo -e "${_pw_FG_R}$@${_pw_CL_RST}"; }
pw_FG() { echo -e "${_pw_FG_G}$@${_pw_CL_RST}"; }
pw_FY() { echo -e "${_pw_FG_Y}$@${_pw_CL_RST}"; }
pw_FB() { echo -e "${_pw_FG_B}$@${_pw_CL_RST}"; }
pw_FM() { echo -e "${_pw_FG_M}$@${_pw_CL_RST}"; }
pw_FC() { echo -e "${_pw_FG_C}$@${_pw_CL_RST}"; }
pw_FW() { echo -e "${_pw_FG_W}$@${_pw_CL_RST}"; }
## Foreground - Bright
pw_FBK() { echo -e "${_pw_FG_BK}$@${_pw_CL_RST}"; }
pw_FBR() { echo -e "${_pw_FG_BR}$@${_pw_CL_RST}"; }
pw_FBG() { echo -e "${_pw_FG_BG}$@${_pw_CL_RST}"; }
pw_FBY() { echo -e "${_pw_FG_BY}$@${_pw_CL_RST}"; }
pw_FBB() { echo -e "${_pw_FG_BB}$@${_pw_CL_RST}"; }
pw_FBM() { echo -e "${_pw_FG_BM}$@${_pw_CL_RST}"; }
pw_FBC() { echo -e "${_pw_FG_BC}$@${_pw_CL_RST}"; }
pw_FBW() { echo -e "${_pw_FG_BW}$@${_pw_CL_RST}"; }

## Background - Normal
pw_BK() { echo -e "${_pw_BG_K}$@${_pw_CL_RST}"; }
pw_BR() { echo -e "${_pw_BG_R}$@${_pw_CL_RST}"; }
pw_BG() { echo -e "${_pw_BG_G}$@${_pw_CL_RST}"; }
pw_BY() { echo -e "${_pw_BG_Y}$@${_pw_CL_RST}"; }
pw_BB() { echo -e "${_pw_BG_B}$@${_pw_CL_RST}"; }
pw_BM() { echo -e "${_pw_BG_M}$@${_pw_CL_RST}"; }
pw_BC() { echo -e "${_pw_BG_C}$@${_pw_CL_RST}"; }
pw_BW() { echo -e "${_pw_BG_W}$@${_pw_CL_RST}"; }
## Background - Bright
pw_BBK() { echo -e "${_pw_BG_BK}$@${_pw_CL_RST}"; }
pw_BBR() { echo -e "${_pw_BG_BR}$@${_pw_CL_RST}"; }
pw_BBG() { echo -e "${_pw_BG_BG}$@${_pw_CL_RST}"; }
pw_BBY() { echo -e "${_pw_BG_BY}$@${_pw_CL_RST}"; }
pw_BBB() { echo -e "${_pw_BG_BB}$@${_pw_CL_RST}"; }
pw_BBM() { echo -e "${_pw_BG_BM}$@${_pw_CL_RST}"; }
pw_BBC() { echo -e "${_pw_BG_BC}$@${_pw_CL_RST}"; }
pw_BBW() { echo -e "${_pw_BG_BW}$@${_pw_CL_RST}"; }

###-----------------------------------------------------------------------------
### @brief
###   Given a string with color escape sequences this remove all of them
###   and gives back a plain text.
### @param $1 - The string to be cleaned-up.
### @returns
###   [echo] The fullpath of the function caller script.
pw_remove_color_sequences()
{
    echo "$1" | sed 's/\x1b\[[0-9;]*m//g';
}


##----------------------------------------------------------------------------##
## Error Handling                                                             ##
##----------------------------------------------------------------------------##
###-----------------------------------------------------------------------------
### @brief Crude error handling trap.
### COWTODO(n2omatt): Make it better.
pw_set_error_handling()
{
    trap 'fatal File: $BASH_SOURCE - Line: $LINENO' ERR
}


##----------------------------------------------------------------------------##
## Logs                                                                       ##
##----------------------------------------------------------------------------##
###-----------------------------------------------------------------------------
pw_func_log()
{
    local FUNC_NAME="${FUNCNAME[1]}";
    echo "$(pw_FBK [$FUNC_NAME])" "$@";
}


###-----------------------------------------------------------------------------
### @brief Exit the script with the message and status 1.
### @param $@ - The message that will be displayed.
pw_log_fatal()
{
    local length=$((${#@} -1));

    echo "$(pw_FR [FATAL]) $1";
    for i in $(seq 1 $length); do
        shift;
        echo -e "        $1";
    done;
    exit 1;
}

###-----------------------------------------------------------------------------
### @param $@ - The message that will be displayed.
pw_log_warning()
{
    local length=$((${#@} -1));

    echo "$(pw_FY [WARNING]) $1";
    for i in $(seq 1 $length); do
        shift;
        echo -e "          $1";
    done;
}


##----------------------------------------------------------------------------##
## Math                                                                       ##
##----------------------------------------------------------------------------##
###-----------------------------------------------------------------------------
pw_math_max()
{
    local A=$1;
    local B=$2;
    test "$A" -gt "$B" && echo "$A" || echo "$B"
}

###-----------------------------------------------------------------------------
pw_math_min()
{
    local A=$1;
    local B=$2;
    test "$A" -lt "$B" && echo "$A" || echo "$B"
}


##----------------------------------------------------------------------------##
## Network                                                                    ##
##----------------------------------------------------------------------------##
##------------------------------------------------------------------------------
pw_network_simple_url_downloader()
{
    local URL="$1";
    local OUTPUT_FILENAME="$2";

    local CURL=$(pw_get_program_path curl);
    if [ -n "$CURL" ]; then
        test -z "$OUTPUT_FILENAME"                      \
            && "$CURL" -L -O                    "$URL"  \
            || "$CURL" -L -o "$OUTPUT_FILENAME" "$URL";

        return $?
    fi;

    local WGET=$(pw_get_program_path wget);
    if [ -n "$WGET" ]; then
        test -z "$OUTPUT_FILENAME"                   \
            && "$WGET"                       "$URL"  \
            || "$WGET" -O "$OUTPUT_FILENAME" "$URL";
        return $?
    fi;

}


##----------------------------------------------------------------------------##
## Options Functions                                                          ##
##----------------------------------------------------------------------------##
###-----------------------------------------------------------------------------
### @brief Gets if the given flag was found in the command line..
### @param $1 - The flag that will be searched - example: -u, --help, -something
### @param $2 - The arguments for the function.
### @returns
###  [real] 0 if could find the flag in the arguments, 1 otherwise.
###  [echo] The flag itself if it was found, an empty string otherwise.
###  COWTODO(n2omatt): Work for short flags too...
###  @hack(stdmatt): Check if we can share some code with pw_getopt_arg.
pw_getopt_exists()
{
    local ARGS_ARR;
    local ARGS_ARR_SIZE;

    pw_array_create ARGS_ARR $@;
    ARGS_ARR_SIZE=$?

    local TARGET_FLAG=${ARGS_ARR[0]}; ## First parameter...
    local TARGET_FLAG_LEN=${#TARGET_FLAG};
    local TARGET_IDX=-1;

    for IDX in $(seq 1 $ARGS_ARR_SIZE); do
        local CURR_ARG=${ARGS_ARR[$IDX]};
        test -z "$CURR_ARG" && continue; ## Empty strings...
        CURR_ARG=$(pw_trim $CURR_ARG);

        ## Gets only the begning of the flag string to
        ## ease the comparisson with the TARGET_FLAG since
        ## both are of same length now.
        STRIPED_ARG=${CURR_ARG:0:$TARGET_FLAG_LEN};
        if [ "$STRIPED_ARG" == "$TARGET_FLAG" ]; then ## Not our flag.
            echo "$TARGET_FLAG";
            return 0;
        fi;
    done

    echo "";
    return 1;
}

###-----------------------------------------------------------------------------
### @brief Gets the value of the given flag.
### @param $1 - The flag that will be searched - example: -u, --help, -something
### @param $2 - The arguments for the function.
### @returns
###  [real] 0 if could find the flag in the arguments, 1 otherwise.
###  [echo] The value of the given flag if was possible to extract it.
###  COWTODO(n2omatt): Work for short flags too...
pw_getopt_arg()
{
    local ARGS_ARR;
    local ARGS_ARR_SIZE;

    pw_array_create ARGS_ARR $@;
    ARGS_ARR_SIZE=$?

    local TARGET_FLAG=${ARGS_ARR[0]}; ## First parameter...
    local TARGET_FLAG_LEN=${#TARGET_FLAG};
    local TARGET_IDX=-1;

    for IDX in $(seq 1 $ARGS_ARR_SIZE); do
        local CURR_ARG=${ARGS_ARR[$IDX]};
        test -z "$CURR_ARG" && continue; ## Empty strings...
        CURR_ARG=$(pw_trim $CURR_ARG);

        ## Gets only the begning of the flag string to
        ## ease the comparisson with the TARGET_FLAG since
        ## both are of same length now.
        STRIPED_ARG=${CURR_ARG:0:$TARGET_FLAG_LEN};
        if [ "$STRIPED_ARG" != "$TARGET_FLAG" ]; then ## Not our flag.
            continue;
        fi;

        ## --flag=value | --flag= value;
        if [ $(pw_string_contains $CURR_ARG "=") ]; then
            ## --flag= value - The argument value is on the next index.
            if [ $(pw_string_ends_with $CURR_ARG "=") ]; then
                TARGET_IDX=$(( $IDX + 1));
                echo ${ARGS_ARR[$TARGET_IDX]};
                return 0;
            fi;

            ## --flag=value - We need to split the string and get the second.
            local SECOND_PART=$(echo $CURR_ARG | cut -d" " -f1);
            echo $SECOND_PART;
            return 0;

        ## --flag value - The argument value is on the next index.
        else
            TARGET_IDX=$(( $IDX + 1));
            echo ${ARGS_ARR[$TARGET_IDX]};
            return 0;
        fi;
    done

    echo "";
    return 1;
}


##----------------------------------------------------------------------------##
## OS Functions                                                               ##
##----------------------------------------------------------------------------##
function PW_OS_OSX()       { echo "osx";       }
function PW_OS_GNU_LINUX() { echo "gnu_linux"; }
function PW_OS_WINDOWS()   { echo "windows";   }
function PW_OS_WSL()       { echo "wsl";       }

function PW_OS_IS_OSX()
{
    if [ "$(pw_os_get_simple_name)" == "$(PW_OS_OSX)" ]; then
        echo "$(PW_OS_OSX)";
        return 0;
    fi;
    echo "";
    return 1;
}

function PW_OS_IS_GNU_LINUX()
{
    if [ "$(pw_os_get_simple_name)" == "$(PW_OS_GNU_LINUX)" ]; then
        echo "$(PW_OS_GNU_LINUX)";
        return 0;
    fi;
    echo "";
    return 1;
}

function PW_OS_IS_WINDOWS()
{
    if [ "$(pw_os_get_simple_name)" == "$(PW_OS_WINDOWS)" ]; then
        echo "$(PW_OS_WINDOWS)";
        return 0;
    fi;
    echo "";
    return 1;
}

function PW_OS_IS_WSL()
{
    if [ "$(pw_os_get_simple_name)" == "$(PW_OS_WSL)" ]; then
        echo "$(PW_OS_WSL)";
        return 0;
    fi;
    echo "";
    return 1;
}

###-----------------------------------------------------------------------------
PW_WSL_AS_GNU_LINUX="PW_WSL_AS_GNU_LINUX";

pw_os_get_simple_name()
{
    _pw_impl_os_get_simple_name "$1";
}


##----------------------------------------------------------------------------##
## Path Utilities                                                             ##
##----------------------------------------------------------------------------##
###-----------------------------------------------------------------------------
pw_get_program_path()
{
    ## @notice(stdmatt): This is needed because the MINGW64_NT-10.0-18362
    ## outputs a lot of non useful garbage when it can't find something.
    ## On GNU or OSX the which(1) doesn't outputs nothing;...
    which "$1" > /dev/null 2>&1;
    if [ "$?" != "0" ]; then
        echo "";
        return -1;
    fi;

    echo $(which "$1");
    return 0;
}

###-----------------------------------------------------------------------------
pw_get_file_mode()
{
    local FILENAME="$1";
    test -z "$FILENAME" && echo "" && return 1;

    echo $(_pw_imp_get_file_mode "$FILENAME");
}

###-----------------------------------------------------------------------------
pw_get_file_owner()
{
    local FILENAME="$1";
    test -z "$FILENAME" && echo "" && return 1;

    echo $(_pw_imp_get_file_owner "$FILENAME");
}

###-----------------------------------------------------------------------------
pw_get_file_group()
{
    local FILENAME="$1";
    test -z "$FILENAME" && echo "" && return 1;

    echo $(_pw_imp_get_file_group "$FILENAME");
}

###-----------------------------------------------------------------------------
pw_can_write_at_path()
{
    test -w "$1";
    return $?
    # ## Based upon: (https://stackoverflow.com/a/14104522).
    # ## Thanks to chepner
    # local DIR="$1";
    # local USER="$2";

    # test ! -n "$DIR" && pw_log_fatal "Dir can't be empty.";
    # test ! -d "$DIR" && pw_log_fatal "Dir ($DIR) is not a valid directory.";

    # if [ -z "$USER" ]; then
    #     USER="$(whoami)";
    # fi;

    # local FILEMODE=$(pw_substr $(pw_get_file_mode "$DIR") -3);
    # local OWNER=$(pw_get_file_owner "$DIR");
    # local GROUP=$(pw_get_file_group "$DIR");

    # ## UserWrite / GroupWrite / OwnerWrite
    # local UW=${FILEMODE:0:1};
    # local GW=${FILEMODE:1:1};
    # local OW=${FILEMODE:2:1};

    # ## @DEBUG
    # ## echo $DIR;
    # ## echo $USER;
    # ## echo "FILEMODE ($FILEMODE)";
    # ## echo "OWNER    ($OWNER)";
    # ## echo "GROUP    ($GROUP)";
    # ## echo "UW ($UW)";
    # ## echo "GW ($GW)";
    # ## echo "OW ($OW)";


    # ## Everyone has write access.
    # if (( ($OW & 02) != 0 )); then
    #     return 0;
    # ## Some group has write access.
    # elif (( ($GW & 02) != 0 )); then
    #     ## Is user in that group?
    #     local gs=( $(groups $USER) );
    #     for g in "${gs[@]}"; do
    #         if [[ $GROUP == $g ]]; then
    #             return 0;
    #         fi;
    #     done;
    # ## The owner has write access.
    # elif (( ($UW & 02) != 0 )); then
    #     ## Does the user own the file?
    #     if [[ $USER == $OWNER ]]; then
    #         return 0;
    #     fi;
    # fi

    # return 1;
}

###-----------------------------------------------------------------------------
pw_get_sudo_path()
{
    echo "$(pw_get_program_path sudo)";
}


###-----------------------------------------------------------------------------
### @brief Get the fullpath of the user home (current user is the default).
### @param $1
###    An [optional] username. \n
###    If a username is given to the function it will retrive the path
###    for user's home. In case that username doesn't exists and empty
###    string is returned instead.
### @returns
###   [echo] The fullpath of the user home. \n
###   [real] 0 if operation is succeeded, 1 otherwise.
pw_get_user_home()
{
    _pw_impl_get_user_home "$1" ## Calls the OS dependent implementation.
}

###-----------------------------------------------------------------------------
### @brief Expands the ~ (if it exists) and make the path absolute.
### @param $1 - The path that will be expanded and made absolute.
### @returns [echo] The processed path.
### @todo(stdmatt): The function isnt' complete yet...
###    Today it's only expanding the ~, but isn't handling the normalization
###    of the path correctly...
pw_expand_user_and_make_abs()
{
    local TARGET_PATH="$(pw_trim $1)";

    ## Starts with a '~', so we need expand the path.
    if [ -n "$(pw_string_starts_with "~" "$TARGET_PATH")" ]; then
        local HEAD=$(echo "$TARGET_PATH" | cut -d "/" -f1);
        local TAIL=$(echo "$TARGET_PATH" | cut -d "/" -f2-1000); ## @hack(stdmatt) replace the magic number.

        local TARGET_USER="$USER";
        if [ "$HEAD" != "~" ]; then ## ~some_user
            TARGET_USER=$(pw_trim_left "$HEAD")
        fi;

        local HOME_PATH=$(pw_get_user_home "$TARGET_USER");
        test -n "$HOME_PATH" && \
            TARGET_PATH="$HOME_PATH/$TAIL";
    fi;

    echo "$TARGET_PATH";
}

###-----------------------------------------------------------------------------
### @brief
###    Gets the fullpath for the bashrc or bash_profile depending on the
###    current platform. The default for OSX is bash_profile and for GNU
###    is bashrc.
### @returns
###    The full path for the "profile".
pw_get_default_bashrc_or_profile()
{
    ## todo(stdmatt): Check if this works on all platforms...
    local CURR_OS=$(pw_os_get_simple_name);
    local USER_HOME=$(pw_find_real_user_home);

    if [ "$CURR_OS" == "$(PW_OS_OSX)" ]; then
        echo "$USER_HOME/.bash_profile";
    elif [ "$CURR_OS" == "$(PW_OS_GNU_LINUX)" ]; then
        echo "$USER_HOME/.bashrc";
    else
        echo "$USER_HOME/.bashrc";
    fi;
}


##----------------------------------------------------------------------------##
## String Functions                                                           ##
##----------------------------------------------------------------------------##
###-----------------------------------------------------------------------------
pw_string_length()
{
    local STR="$1";
    echo "${#STR}";
}

###-----------------------------------------------------------------------------
pw_string_in()
{
    local NEEDLE="${@:1:1}";
    local HAYSTACK="${@:2:10000}"; ## @magic(stdmatt): Remove the magic numbers.
    for ITEM in $HAYSTACK; do
        if [ "$NEEDLE" == "$ITEM" ]; then
            echo "$NEEDLE";
            return 0;
        fi;
    done;
    echo "";
    return 1;
}

###-----------------------------------------------------------------------------
### @brief Checks if a string constains the other string.
### @param $1 - The haystack string.
### @param $2 - The needle string.
### @returns 0 if found, 1 otherwise.
pw_string_contains()
{
    local RET_VAL=$(echo "$1" | grep "$2");
    test -z $RET_VAL && return 1;
    return 0;
}

###-----------------------------------------------------------------------------
### @todo(stdmatt): Add docs...
pw_string_replace()
{
    local TARGET_STR="$1";
    local WHAT="$2";
    local TO="$3";

    test -z "$1" && return 1; ## Empty target string.
    test -z "$2" && return 1; ## Nothing to replace...

    RET_VAL=$(echo "$TARGET_STR" | sed s/"$WHAT"/"$TO"/g);

    echo "$RET_VAL";
    return 0;
}

###-----------------------------------------------------------------------------
### @todo(stdmatt): Add docs...
pw_string_starts_with()
{
    local HAYSTACK="$1";
    local NEEDLE="$2";

    local GREP_RES=$(echo "$HAYSTACK" | grep "^$NEEDLE");
    if [ -n "$GREP_RES" ]; then
        echo "$HAYSTACK";
        return 0;
    fi;

    echo "";
    return 1;
}

###-----------------------------------------------------------------------------
### @todo(stdmatt): Add docs...
pw_string_ends_with()
{
    local HAYSTACK="$1";
    local NEEDLE="$2";

    local GREP_RES=$(echo "$HAYSTACK" | grep "${NEEDLE}$");
    if [ -n "$GREP_RES" ]; then
        echo "$HAYSTACK";
        return 0;
    fi;

    echo "";
    return 1;
}

###-----------------------------------------------------------------------------
### @brief Check if a string represents a number.
### @param $1 - The number string.
### @returns - 0 if string represents a number, 1 otherwise.
pw_is_number()
{
    test -z "$1" && return 1;

    CLEAN_VAL=$(echo "$1" | sed "s/[^[:digit:].-]//g");

    test "$1" == "$CLEAN_VAL" && return 0;
    return 1;
}




##----------------------------------------------------------------------------##
## Text Utilities                                                             ##
##----------------------------------------------------------------------------##
###-----------------------------------------------------------------------------
pw_substr()
{
    local STR="$1";
    test -z "$STR" && echo "" && return 0;

    local STR_LEN=$(pw_strlen "$STR");

    local FIRST_INDEX="$2";
    pw_is_number "$FIRST_INDEX";
    if [ $? != 0 ]; then
        pw_log_fatal "[pw_substr] First index is not a number - Found ($FIRST_INDEX)";
    elif [ $FIRST_INDEX -lt 0 ]; then
        FIRST_INDEX=$(( STR_LEN + FIRST_INDEX ));
    fi;

    local SECOND_INDEX="$3";
    if [ -n "$SECOND_INDEX" ]; then
        pw_is_number "$SECOND_INDEX";
        if [ $? != 0 ] ; then
            pw_log_fatal "[pw_substr] Second index is not a number - Found ($SECOND_INDEX)";
        elif [ $SECOND_INDEX -lt 0 ]; then
            SECOND_INDEX=$(( STR_LEN + SECOND_INDEX ));
        fi;
    else
        SECOND_INDEX="$STR_LEN";
    fi;
    SECOND_INDEX=$(( FIRST_INDEX + SECOND_INDEX ))

    ## @DEBUG
    ## echo "STR          $STR";
    ## echo "LEN          $STR_LEN";
    ## echo "FIRST_INDEX  $FIRST_INDEX";
    ## echo "SECOND_INDEX $SECOND_INDEX";
    ## echo "-->${STR:$FIRST_INDEX:$SECOND_INDEX}<--"

    echo "${STR:$FIRST_INDEX:$SECOND_INDEX}";
}

###-----------------------------------------------------------------------------
pw_strlen()
{
    local STR="$1";
    local LEN="${#STR}";

    echo $LEN;
}


###-----------------------------------------------------------------------------
### @brief Counts how many words the string has.
### @param $1 - An [optional] string.
### @note We define a word here a piece of text separated by spaces.
### @returns
###   [echo] The number of words, or 0 if no text is given. \n
###   [real] 0 if operation is succeeded, 1 otherwise.
pw_count_words()
{
    ## Empty...
    test -z "$1" && echo "0";

    local ARGS="$@"
    local RESULT=0;
    for _ in $ARGS; do
        RESULT=$(( RESULT + 1 ));
    done;

    echo "$RESULT";
    return 0;
}


###-----------------------------------------------------------------------------
### @brief Trim the leading chars in the string.
### @param $1 - An [optional] string.
### @param $2 - An [optional] character, default is space.
### @returns
###   [echo] The trimmed string. \n
###   [real] 0 if operation is succeeded, 1 otherwise.
pw_trim_left()
{
    local TEXT_STR="$1";
    if [ -z "$TEXT_STR" ]; then ## No text, no need to trim anything...
        echo "";
        return 0;
    fi;

    local TEXT_STR_LEN="${#TEXT_STR}";
    local TRIM_CHAR="$2";
    test -z "$TRIM_CHAR" && TRIM_CHAR=" "; ## No given char, set the default.

    ## Find the first char that isn't our trim char.
    local BEGIN_INDEX=0;
    for (( i=0; i < $TEXT_STR_LEN; ++i )); do
        local CURR_CHAR="${TEXT_STR:i:1}";
        test "$CURR_CHAR" != "$TRIM_CHAR" && break;
        BEGIN_INDEX=$(( $i + 1 ));
    done;

    echo ${TEXT_STR:BEGIN_INDEX:$(( TEXT_STR_LEN - BEGIN_INDEX))};
    return 0;
}

###-----------------------------------------------------------------------------
### @brief Trim the trailing chars in the string.
### @param $1 - An [optional] string.
### @param $2 - An [optional] character, default is space.
### @returns
###   [echo] The trimmed string. \n
###   [real] 0 if operation is succeeded, 1 otherwise.
pw_trim_right()
{
    local TEXT_STR="$1";
    if [ -z "$TEXT_STR" ]; then ## No text, no need to trim anything...
        echo "";
        return 0;
    fi;

    local TEXT_STR_LEN="${#TEXT_STR}";
    local TRIM_CHAR="$2";
    test -z "$TRIM_CHAR" && TRIM_CHAR=" "; ## No given char, set the default.

    ## Find the last char that isn't our trim char.
    local BEGIN_INDEX=0;
    local END_INDEX="$(( TEXT_STR_LEN -1 ))";
    for (( i=$(( TEXT_STR_LEN -1 )); i > $BEGIN_INDEX; --i )); do
        local CURR_CHAR="${TEXT_STR:i:1}";
        test "$CURR_CHAR" != "$TRIM_CHAR" && break;
        END_INDEX=$(( $i ));
    done;

    echo "${TEXT_STR:$BEGIN_INDEX:$(( END_INDEX + 1 ))}";
    return 0;
}

###-----------------------------------------------------------------------------
### @brief Trim the chars in the string.
### @param $1 - An [optional] string.
### @param $2 - An [optional] character, default is space.
### @returns
###   [echo] The trimmed string. \n
###   [real] 0 if operation is succeeded, 1 otherwise.
pw_trim()
{
    echo $(pw_trim_right $(pw_trim_left "$1" "$2") "$2");
    return 0;
}


###-----------------------------------------------------------------------------
### @brief Makes all string's chars lowercase.
### @param $1 - An [optional] string.
### @returns
###   [echo] The lowercased string. \n
###   [real] 0 if operation is succeeded, 1 otherwise.
pw_to_lower()
{
    echo $(echo $1 | tr [:upper:] [:lower:]);
    return 0;
}

###-----------------------------------------------------------------------------
### @brief Makes all string's chars lowercase.
### @param $1 - An [optional] string.
### @returns
###   [echo] The lowercased string. \n
###   [real] 0 if operation is succeeded, 1 otherwise.
pw_to_upper()
{
    echo $(echo $1 | tr [:lower:] [:upper:]);
    return 0;
}


##----------------------------------------------------------------------------##
## User                                                                       ##
##----------------------------------------------------------------------------##
###-----------------------------------------------------------------------------
### @brief
###   Performs the next operation as super user if needed, i.e. the
###   privileges only will be elevated if there's a need to do so.
### @param $@ - Operation arguments...
pw_as_super_user()
{
    local SUDO="";
    if [ $UID != 0 ]; then
        SUDO="$(pw_get_sudo_path)";
    fi;

    $SUDO "$@";
}

###-----------------------------------------------------------------------------
### @brief
###   Gets the "real" user's home path. \n
###   This will works even if the user is using sudo to run the script.
### @returns
###   [echo] The the real user's home path. \n
pw_find_real_user_home()
{
    local REAL_USER_HOME="";

    if [ $UID == 0 ]; then
        local USER=$(printenv SUDO_USER);
        if [ -z "$USER" ]; then
            REAL_USER_HOME="$HOME";
        else
            REAL_USER_HOME="$(pw_get_user_home $USER)";
        fi;
    else
        REAL_USER_HOME="$HOME";
    fi;

    echo "$REAL_USER_HOME";
}

##----------------------------------------------------------------------------##
## Unix Tools Replacements                                                    ##
##----------------------------------------------------------------------------##
###-----------------------------------------------------------------------------
pw_killall()
{
    local PROCESS_TO_KILL="$1";
    test -z "$PROCESS_TO_KILL" &&                \
        pw_log_warning "No process to kill."  && \
        return 1;

    ## TODO(stdmatt): Accept comamnd line options...
    ## Today we are just getting the process name, so there's no way to
    ## pass flags to kill(1) currently.
    local TEMP_DIRPATH="/var/tmp";
    local TEMP_FULLPATH="${TEMP_DIRPATH}/pw_killall.txt";
    mkdir -p "$TEMP_DIRPATH";

    ps aex | grep "$PROCESS_TO_KILL" > "$TEMP_FULLPATH";
    while read LINE; do
        local PID=$(echo "$LINE" | cut -d " " -f1);
        echo "Killing PID: ($PID)";
        kill "$PID";
    done < $TEMP_FULLPATH;
}

###-----------------------------------------------------------------------------
### @brief Emulates the realpath in systems that don't have one.
### @note
###    In systems, like GNU, that has realpath, this function will
###    just forward the arguments to it.
pw_realpath()
{
    ##
    ## We have the realpath(3), so just forward.
    local REALPATH=$(pw_get_program_path realpath);
    if [ -n "$REALPATH" ]; then
        $REALPATH $@
        return;
    fi;

    ##
    ## We need to emulate it.
    ##   Thanks for Geoff [https://stackoverflow.com/a/18443300]
    local OLD_PWD="$PWD";

    cd "$(dirname "$1")"
    local LINK=$(readlink "$(basename "$1")");

    while [ "$LINK" ]; do
      cd "$(dirname "$LINK")";
      LINK=$(readlink "$(basename "$1")");
    done;

    local RESULT="$PWD/$(basename "$1")";

    cd "$OLD_PWD";
    echo "$RESULT";
}

##------------------------------------------------------------------------------
pw_abspath()
{
    local _PATH=$(pw_realpath $1);
    local CURR_PATH="$_PATH";
    ## /Users/stdmatt/Documents/Projects/stdmatt/games/flappy_gb/scripts/../game/include/res/flappy.c
    local FINAL_PATH="";
    # echo "_PATH          $_PATH"
    while true; do
        local CURR_COMPONENT=$(basename "$CURR_PATH")
        if [ "$CURR_COMPONENT" == "." ]; then
            CURR_PATH=$(dirname "$CURR_PATH");
        elif [ "$CURR_COMPONENT" == ".." ]; then
            CURR_PATH=$(dirname $(dirname "$CURR_PATH"));
        else
            FINAL_PATH="${CURR_COMPONENT}/${FINAL_PATH}";
            CURR_PATH=$(dirname "$CURR_PATH");
        fi;

        if [ "$CURR_COMPONENT" == "/" ]; then
            break;
        fi;

        # echo "CURR_COMPONENT $CURR_COMPONENT"
        # echo "CURR_PATH      $CURR_PATH"
        # echo "FINAL_PATH     $FINAL_PATH"
    done;


    if [ -n "$(pw_string_starts_with "$FINAL_PATH" "//")" ]; then
        FINAL_PATH=$(pw_substr $FINAL_PATH 1);
    fi;

    if [ -n "$(pw_string_ends_with "$FINAL_PATH" "//")" ]; then
        FINAL_PATH=$(pw_substr $FINAL_PATH 0 -2);
    elif [ -n "$(pw_string_ends_with "$FINAL_PATH" "/")" ]; then
        FINAL_PATH=$(pw_substr $FINAL_PATH 0 -1);
    fi;
    echo $FINAL_PATH
}

##------------------------------------------------------------------------------
pw_watch()
{
    local WATCH=$(pw_get_program_path "watch");
    test -n "$WATCH" \
        && $WATCH $@ \
        && return 0;

    local TIME_FLAG="$1";
    local DELAY_TIME="2";
    local ARGS="$@";

    ## Check if we passed the delay flag.
    if [ "$TIME_FLAG" == "-n" ]; then
        DELAY_TIME="$2";
        ## Now check if the delay argument is a valid number
        pw_is_number "$2";
        if [ $? == 0 ]; then
            ARGS="${@:3:1000}"; ## @MAGIC(stdmatt): 1000 is to get all values...
        else
            echo "invalid delay argument";
            return 1;
        fi
    fi;

    ## Actually run the command...
    while true; do
        clear;
        date;
        $ARGS | head -10
        sleep $DELAY_TIME;
    done;
}

##------------------------------------------------------------------------------
pw_pushd()
{
    pushd "$1" > /dev/null;
}

##------------------------------------------------------------------------------
pw_popd()
{
    popd > /dev/null;
}
